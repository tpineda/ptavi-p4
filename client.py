#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente UDP que abre un socket a un servidor."""
import sys
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
try:
    direccionip = sys.argv[1]
    puerto = int(sys.argv[2])
    register = sys.argv[3]
    correo = sys.argv[4]
    expiracion = int(sys.argv[5])

except SyntaxError:
    sys.exit("Usage: client.py ip puerto register sip_address expires_value")

solicitud = 'REGISTER SIP: ' + correo + 'SIP/2.0\r\n\r\n'
solicitud += 'expires in: ' + str(expiracion) + '\r\n\r\n'

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((direccionip, puerto))
    print("Enviando:", solicitud)
    my_socket.send(bytes(solicitud, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")
